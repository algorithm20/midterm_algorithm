/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.algorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author acer
 */
public class midterm_Josephus {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Scanner kb = new Scanner(System.in);
        String namecheck = kb.next();
        File file = new File("C:\\code\\inout_Josephus\\input_ep1.txt");
        Scanner inputData = new Scanner(checkInput(namecheck, file));
        ArrayList<String> person = new ArrayList<>();
        addDataToPerson(person, inputData );
        startGame(person,0,5);
        System.out.println("Survival is: "+person);
        
    }

    static File checkInput(String namecheck, File file) throws IOException {
        if (namecheck.equals("1.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep1.txt");
        } else if (namecheck.equals("2.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep2.txt");
        } else if (namecheck.equals("3.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep3.txt");
        } else if (namecheck.equals("4.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep4.txt");
        } else if (namecheck.equals("5.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep5.txt");
        } else if (namecheck.equals("6.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep6.txt");
        } else if (namecheck.equals("7.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep7.txt");
        } else if (namecheck.equals("8.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep8.txt");
        } else if (namecheck.equals("9.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep9.txt");
        } else if (namecheck.equals("10.in")) {
            return file = new File("C:\\code\\inout_Josephus\\input_ep10.txt");
        }

        return file;
    }

    static ArrayList<String> addDataToPerson(ArrayList<String> list, Scanner in) {
        while (in.hasNext()) {
            list.add(in.next());
        }
        return list;
    }

    static String startGame(ArrayList<String> person, int  index, int k) {
       
        if (person.size() == 1) {
            return person.get(0);
        }
        index = (index + k) % person.size(); 
        person.remove(index);
        //startGame(person, k);
        return startGame(person,index, k);
    }

}
